#ifndef __UnZip__Zip__
#define __UnZip__Zip__

//
//  Zip.h
//  Zip
//
//  Created by Nathan Wehr on 9/16/14.
//  Copyright (c) 2014 EvriChart, Inc. All rights reserved.
//

// C
#include <zlib.h>

// C++
#include <string>
#include <sstream>
#include <exception>

// Eloquent
#include "Eloquent/Extensions/Filters/Filter.h"

namespace Eloquent {
	class UnZip : public Filter {
	public:
		UnZip( const boost::property_tree::ptree::value_type& i_Config )
		: Filter( i_Config )
		{}
		
		virtual ~UnZip() {}
		
		std::string decompress_string(const std::string& str)
		{
			z_stream zs;                        // z_stream is zlib's control structure
			memset(&zs, 0, sizeof(zs));
			
			if (inflateInit(&zs) != Z_OK)
				throw(std::runtime_error("inflateInit failed while decompressing."));
			
			zs.next_in = (Bytef*)str.data();
			zs.avail_in = str.size();
			
			int ret;
			char outbuffer[32768];
			std::string outstring;
			
			// get the decompressed bytes blockwise using repeated calls to inflate
			do {
				zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
				zs.avail_out = sizeof(outbuffer);
				
				ret = inflate(&zs, 0);
				
				if (outstring.size() < zs.total_out) {
					outstring.append(outbuffer,
									 zs.total_out - outstring.size());
				}
				
			} while (ret == Z_OK);
			
			inflateEnd(&zs);
			
			if (ret != Z_STREAM_END) {          // an error occurred that was not EOF
				std::ostringstream oss;
				oss << "Exception during zlib decompression: (" << ret << ") "
				<< zs.msg;
				throw(std::runtime_error(oss.str()));
			}
			
			return outstring;
		}
		
		virtual bool operator<<( std::string& io_Data ) {
			try {
				io_Data = decompress_string( io_Data );
				
				return Continue( true );
				
			} catch( ... ) {
				return Continue( false );
			}
			
		}
		
	};
	
}

#endif /* defined(__Zip__Zip__) */
