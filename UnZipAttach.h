#ifndef __Zip__ZipAttach__
#define __Zip__ZipAttach__

//
//  ZipAttach.h
//  Zip
//
//  Created by Nathan Wehr on 9/16/14.
//  Copyright (c) 2014 EvriChart, Inc. All rights reserved.
//

extern "C" void* Attach(void); 

#endif /* defined(__Zip__ZipAttach__) */
